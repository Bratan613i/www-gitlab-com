---
layout: handbook-page-toc
title: Team Member Social Media Advocacy
description: Strategies and details to enable team members to share GitLab-related news on personal social media channels
twitter_image: /images/opengraph/handbook/social-marketing/team-member-social-advocacy-opengraph.png
twitter_image_alt: GitLab's Social Media Handbook branded image
twitter_site: gitlab
twitter_creator: gitlab
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Team Member Social Media Advocacy and Enablement

[According to Sprout Social](https://sproutsocial.com/insights/what-is-employee-advocacy/), team member advocacy is the promotion of an organization by its staff members. People trust recommendations and content from people they know. They trust these people a lot more than they trust marketing messages from companies on organic brand social channels or paid social media advertising. We know this when a team member can post the same content the brand channel publishes, but because the message was more personal, more human, the team member gains significant engagement over the brand channel.

### Social Advocacy Strategy

To enable all GitLab team members to confidently and comfortably share GitLab related stories on their personal social media channels in a way that is risk adverse yet provides measureable performance. 

#### Goals of social media advocacy 

- Improve the organic reach of GitLab messaging
- Increase traffic to our site from social media, particularly to blogs and campaign content
- Reduce brand and team member risk by providing curated content to publish
- Secondary: aid in sales processes for social selling initiatives 
- Secondary: drive recruitment to accelerate hiring values-drive team members

#### Benefits of social advocacy to all team members

- Curated and prewritten drafts for social posts allow for easy publishing with minimal edits required
- Posts can be scheduled, like a social media manager would for the brand, allowing the work to be semi-automated
- Easy way to include incentives like swag, bonuses, or other add-ons
- Career growth: sharing industry content on social media can help make team members be looked at as thought leaders in their spaces - this boosts your personal brand
- Save time - by checking Bambu on a regular cadence and scheduling content on your channels for a week or two, you'll be enabled to share a lot of GitLab stories without the need to spend a lot of time sourcing, writing, and manually posting on your own social channels

### Bambu, our social advocacy tool

Bambu by Sprout Social is an employee advocacy platform for you to share content across LinkedIn, Twitter or Facebook. Bambu enables you to quickly and easily share content on LinkedIn, Twitter and Facebook to amplify our brand reach and help establish your personal brand on social media.The goal of this tool is to centralize content that is valuable to our audience.

<details><summary>How do I get started?</summary>

<p>
First, you'll need to confirm that you have been assigned the Bambu application in Okta in order to use it. If you are not assigned Bambu in Okta and you are interested, please reach out to the team in the #social-advocacy Slack channel.
</p>
<p>
You'll need to log into Okta and locate the Bambu logo tile. Click on the Okta tile and you'll automatically be logged in for the first time. You'll be promoted to confirm a few items and that's it! You'll always have access to Bambu via logging in with Okta.
</p>
<p>
Once you've logged in for the first time, save a bookmark in your browser for <a href="https://gitlab.getbambu.com/login">https://gitlab.getbambu.com/login</a>
</p>
<p>
Click the login with SSO option at the bottom of the page
</p>

</details>

<details><summary>What am I supposed to do inside Bambu?</summary>

<p>
When you log into Bambu, you will see a collection of stories curated specifically for you.
</p>
<p>
This is a centralized hub for you to learn, build your reputation online, and help spread the word about GitLab by sharing these stories with your networks on Facebook, LinkedIn, and Twitter. We made it easy to share, too.
</p>
<p>
When you click the share icon on any story, you’ll see that we’ve added a suggested status update. Of course, you’re welcome to adjust or completely rewrite it to match your own voice (the only exception to this will be when you’re sharing content that needs to be worded a certain way for compliance reasons, in which case, we'll say so in the notes). However, sharing to Twitter requires customizing a minimum of 20 characters in the copy suggestions we provide. This is because Twitter would otherwise see a lot of tweets with the exact same text and each of these posts would be marked as spam and recieve no impressions, engagements, or clicks. This is a feature of Bambu. When you go to schedule a tweet, you'll see a note requiring you to update the copy.
</p>

</details>

<details><summary>How often am I supposed to login to Bambu?</summary>

<p>
As often as you like, but try to make a habit of logging in at least once a week, as we are constantly adding new and useful information.
</p>
<p>
We will make sure that any time-sensitive stories find their way to you through Slack, like in the #whats-happening-at-gitlab channel. 
</p>
<p>
There are other ways to stay in touch with the latest stories to share:
</p>

<p>
<a href="https://slack.com/blog/productivity/make-it-a-habit-periodic-reminders-for-slack">Create a recurring Slack reminder</a> to check Bambu once a week or every other week
</p>

<p>
If you run your workday via your calendar, consider adding a 25 min block once a week or every other week to login and share the latest stories
</p>

</details>

<details><summary>Can I automate Bambu?</summary>

<p>
It’s possible to spend less than 25 minutes every time you log into Bambu to schedule content on your social media channels for a week or two. <a href="https://bambu.zendesk.com/hc/en-us/articles/360004450291-Sharing-a-Story">We highly recommend taking advantage of the “Send Later” button feature</a> when you want to share a story. 
</p>

</details>

#### Team Member Roles in Bambu, the advocacy program

<details><summary>Admins</summary>

<p>
The GitLab Social Team are the administrators of the social advocacy program. Admins have all access to our tool, Bambu, as needed to operate the program. Admins may act as curators from time to time as well.
</p>
<p>
 If you have questions or would like to learn more, consider sending a message in the #social-advocacy Slack channel.
</p>

<table>
  <tr>
   <td>Name
   </td>
   <td>Role
   </td>
  </tr>
  <tr>
   <td>Kristen Sundberg
   </td>
   <td>Curator and Program Admin
   </td>
  </tr>
  <tr>
   <td>Wil Spillane
   </td>
   <td>Technology Owner and Admin
   </td>
  </tr>
</table>

</details>

<details><summary>Curators (or Contributors)</summary>

<p>
Curators are selected intentionally to drive our advocacy content strategy and specific team members are asked to take on the role of a curator as a part of their everyday jobs at GitLab. We will have curators representing all areas of GitLab the brand and the product in order to curate a list of related content worth sharing on social media. 
</p>
<p>
Curators have all Reader access as well as the ability to curate stories and submit them to a Manager or Admin for approval. 
</p>
<p>
Join the #social-advocacy-curators Slack channel to stay in touch with the curator program and the latest news. This channel is intended for team members who are identified as content curators only.
</p>
<p>
<a href="https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/social-advocacy-curators/">If you're a curator or interested in becoming a curator, head to our curator-specific handbook page here.</a>
</p>

</details>

<details><summary>General Team Members (or Readers)</summary>

<p>
All team members can access the Stories feed, share to their social networks and leverage the Suggestions feature to submit links to a Manager or Admin for complete Story curation.
</p>
<p>
Access to Bambu is provided via an Okta tile - please log in to Okta and find the Bambu logo tile to Log on.
</p>
<p>
Join the #social-advocacy Slack channel to stay in touch with the program and the latest news.
</p>

</details>

### Suggesting content for team members to share on Bambu

While we have a team of content curators working to add relevant and fresh content for the whole team to share on a regular basis, you may come across content that you'd like us to consider adding to Bambu, mainly content from 3rd party sources: your own blogs or a partner website link. Here's how to suggest content to an admin to add to Bambu. 

- Click the +Suggest Story button in the upper right hand corner
- Paste the story URL and click Suggest Story
- The admin team will review your suggestion. If we approve it, we'll mark you as the curator for the content.

Please note that most of what is approrpriate to share that comes from GitLab will be curated already.

### Scheduling stories in advance with Bambu

Scheduling stories on your social media channels is the best way to automate some of the manual work behind promoting content on social media. While it still requires you to check out Bambu and to edit the copy suggestions, you can "set it and forget it" and bulk a week or two worth of social posts in one short period of time. This is how the scheduling feature looks in Bambu.

<img src="/images/handbook/marketing/corporate-marketing/bambu-scheduling-screenshot.png" alt="Sreenshot of Bambu scheduling feature">

Consider scheduling posts during your timezones regular business hours --- at the start of the work day (7am - 10am) around lunch time (11am - 1pm) and just after the end of the day (5pm - 7 pm). What works for your followers may also be different - it's important to check out post performance and to try out different times to see which would work best. Sorry, Bambu doesn't provide "best time of day" for scheduling posts.

### Integrations

- We are actively working to include link attribution for Salesforce and Marketo, to better identify how shared content on social channels enabled the sales process.

#### Slack and Bambu

The Slack integration will allow us to broadcast a Bambu story link to Slack, giving our team the headsup on big news items that we'd like to share. This will allow GitLab to continue operating like GitLab. You can also check out the latest stories on Bambu inside of any Slack channel by using `/bambu` for a private message showing you our top stories.

<img src="/images/handbook/marketing/corporate-marketing/bambu-slack-stories-screenshot.png" alt="Sreenshot of Bambu stories in Slack">


#### Okta and Google Groups

Access to Bambu is provisioned via Okta. And Okta app assignments are provisioned from our [okta-bambu-users Google Group](https://groups.google.com/a/gitlab.com/g/okta-bambu-users/members). In order to get access as a curator or user of Bambu, the team member will need to be added to the okta-bambu-users Google Group. This group then automatically assigns the Bambu app access in the team member's Okta within the next hour or so. 

The reverse is also true: when a team member is removed from the okta-bambu-users Google Group, they will be deprovisioned from the Okta app assignment and therefore from Bambu. As a part of the all team member offboarding process, we've identified the deprovisioning necessary for Bambu access. @wspillane is assigned every offboarding issue and follows this workflow:
- Review the offboarding issue for team member name
- Review team member access in the okta-bambu-users Google Group
   - If the team member is _not_ in the Google Group: head back to the offboarding issue and select the `Not Applicable to Team Member` box - no further instructions
   - If the team member is in the Google Group: Remove team member from the okta-bambu-users Google Group - continue actions below
- Removing the team member from the group will deprovision app assignment in Okta, eliminating their account in Bambu
- Check off the `Access Deprovisioned` box. 

### Contests

We'll run contests for team members from time to time to enable more sharing across particular topics or campaigns. You'll find out about these contests in the #whats-happening-at-gitlab Slack channel.

### Reporting and Metrics 

[Bambu provides a report center to outline all of the possibilities here](https://bambu.zendesk.com/hc/en-us/articles/360020038351-Bambu-Report-Guide).

Conversion Rate - the percentage of team members invited to the program that are actually participating

Active Participation - the percentage of team members that are engaged in the program and sharing on any given reporting period (week/month/quarter)

Top Contributors identifying top contributors helps to understand the kind of content that will work best and recognizing top contributors is a good way to keep the program engaging

Organic reach - the number of people seeing content shared through Bambu by our team member advocates

Engagement - measuring the number of actions taken on content shared through Bamby by our team member advocates (think likes, clicks, comments, and shares)

Ad value or equivalency - similar to how we measure this for the brand, this is measured in a dollar value for advertising determined by the sum of a reporting period's equivalent CPM + CPC costs

### Bambu FAQs

##### Why did we choose Bambu over [some other platform]?

There are dozens of employee social media advocacy tools, many of which operate similarly. We chose Bambu due to the deep ties with our existing social media management software, Sprout Social, and the potential to streamline the "brand-to-team-member" content process. It's also pretty easy to use. 

##### I was not able to provide feedback on the tool selection process, the rollout of this program, or the topics and strategy. What gives?

This project has operated inside of the social team for a long time and has had to overcome many challenges from across the company in order to launch. We're thrilled that you'd want to provide feedback, however, we've developed the program with the right team members attached and providing feedback. For your ideas, please consider opening an issue for a future iteration where the team can consider your ideas to be included at a later time. 

##### Will Bambu replace the use of issues for social media enablement for team members?

Yes, that was a part of the original goal. Using GitLab issues for social enablement is an extremely manual process that has not been efficent. Bambu will allow a centralized and easy to use administration to the entire company while also helping us to understand performance metrics. 

##### What personally identifiable data is available for GitLab?

GitLab is not given any personal information about your social media profiles, actions, comments, DM, or any other element. When you connect your social media channels to Bambu it provides a way for you to schedule and publish content on your social channels automatically using all of the APIs available on these social channels. It also provides a top level, no context review of performance metrics on *posts you published on your personal social media channels through Bambu only. 

The data that GitLab receives shows your posts published through Bambu: 
- Reach - or how many people saw the posts you published through Bambu
- Engagements - or how many actions people took on the posts you published through Bambu; comments, likes, shares, etc. 
- Clicks - or how many times people clicked on the link you published in your post through Bambu

*GitLab cannot see who liked your posts, what their comments were on your posts, who shared your posts, or any information relating to posts that are not published through Bambu (so all of your personal activity is still personal).*

Here is a look at the data that is personally identified with individual team members. Note again that we cannot see the context behind the data and this is only for content that is scheduled and published through Bambu.

<img src="/images/handbook/marketing/corporate-marketing/Bambu-Data-1.png" alt="Sreenshot of Bambu data">

##### Why are preview images, titles, and text not included within shared posts?

There may be an issue with the page you're trying to use for the story. Preview images, titles, and text are dictated by the data in the frontmatter of the page, and we may not have control over what appears here. [Learn more about how to determine what information gets pulled by social channels here.](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/admin/#data-needed-on-pages-in-order-for-links-to-work-on-social-media)

##### Can I share content that is not alredy in Bambu through Bambu for better reporting? 

No, we do not have this feature activated and it is not on our roadmap.

##### Can I suggest individual pieces of content be added to Bambu?

Yes, there is a suggestion feature built into the tool. You'll see a `Suggest Story` button on the top left of your screen. However, we have an active group of curators who are responsbile for curating content. We are asking that the team keep their suggestions to away from [topics that are already accounted for with DRIs](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/social-advocacy-curators/#current-list-of-curators). Please connect with the DRI for a topic if you believe a piece of content should be added, since they are the responsible party on their assigned topic. Applying a team member's suggestion is entirely at the discretion of the social team and/or the assigned DRI for a specific topic and is subjective to what else is being promoted, if there are more important pieces of content for that topic, etc. 

Please keep in mind that providing a suggestion is only the first piece of the work necessary for the content to appear for the entire team to share on Bambu. We'd still need to vet the content for standards, legal review, write copy for the posts, and release it to be available in the platform. Adding suggestions adds more work for the social team and our team of curators, so make it worth it!

##### Is Bambu connected to Salesforce or Marketo?

While Bambu offers the possibility to connect with both Salesforce and Marketo for appropriate attribution, these features have not yet been activated. Issues for connecting [Salesforce](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/1635) and [Marketo](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/4739) are opened and need to be completed. If you believe these connections are important for your use of Bambu, please reach out to me and the other stakeholders in the appropriate issue linked above.

##### Where does the content in Bambu come from? 

The content in Bambu comes from a group of GitLab team members we call curators, they help us to source and promote content that is important in their line of work. Folks from the security team will provide security related content, our all remote team will provide content about remote work, and so on. If there is a focus that is not being represented and it would provide content that the team actually wants to promote, please reach out in #social-advocacy-curators Slack channel with your idea. Please note that in the first few months of this program launch, we are not prioritizing more topics and more curators, so while the idea might be great, we'll need to add any additional thoughts to a future iteration. [Learn more about our curator program](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/social-advocacy-curators/).

##### I don't see anything about [your topic of choice] in Bambu, why is that?

This could be a for a number of reasons. If the topic is already covered in Bambu, the DRI may not have gotten to review this content yet or may have deemed other pieces more important to include. If the topic is not already covered in Bambu, this would be why it's not on the platform. At this point, we could discuss it further if you reach out in #social-advocacy-curators Slack channel with your idea. Please note that in the first few months of this program launch, we are not prioritizing more topics and more curators, so while the idea might be great, we'll need to add any additional thoughts to a future iteration. [Learn more about our curator program](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/social-advocacy-curators/).

##### What's in the next iteration of Bambu and social advocacy?

Sales: We're interested in getting our sales team and their sales-adjacent partners access to content that will help them drive better results as well as plug into the tools they use, like Marketo and Salesforce, for proper attribution data. 

Talent Brand: We'd like to work with the talent aquisition and talent brand teams to curate more employer brand content that could lend in securing better talent in our pipeline. 

If you are in one of these teams and would like to volunteer your time to build these iterations out, please reach out to the social team in the  #social-advocacy-curators Slack channel. 

## Team member social media guidance

Learning from the sections below and adding your learnings to the practices of using Bambu for GitLab-related social media posts, you'll become a company champion on social media. Check out some of our concepts below and always feel free to reach out to the #social-advocacy Slack channel with any questions.

### You ought to promote your public-facing work

If you've written a blog for our site, contributed to our latest release, or joined a webinar/webcast, you should want to tell your networks about it. Not only does this provide a way to build your following and expertise in the public domain, but it's also a great way to add critical promotion to your work. Promoting on social media isn't just about the GitLab brand channels. It's an orchestra of efforts, which includes team member support and advocacy.

### Use your voice

When responding to posts from your account, feel free to incorporate your style and voice. Talk to people as if you were talking to them in person. Be sure to speak with “we” and not "I" (as often as appropriate) to represent the company and the community.

Someone doesn’t like something? Ask them to tell us more in the issue tracker. Someone thinks GitLab could be better? Invite them to submit a feature proposal. Any criticism is an opportunity to improve in our next iteration.

Please do not engage in competitor bashing. Instead, highlight positive differences — it's best to focus on the ways that GitLab outperforms other solutions.

### Dealing with conflict and avoiding arguments

You may come across angry users from time to time. When dealing with confrontational people, it’s important to remain level-headed. Sometimes, the best course of action is to walk away and not engage with the person at all. Use your judgment in how you approach rude or off-putting comments from strangers in real life to help you decide.

For a foundational understanding of these nuances, read [GitLab's guide to communicating effectively and responsibly through text](/company/culture/all-remote/effective-communication/).

- Assume good faith. People have opinions and sometimes they’re strong ones. It’s usually not personal.
- If it’s getting personal, step away from the conversation and delegate to someone else.
- Sometimes all people need is acknowledgment. Saying “Sorry things aren’t working for you” can go a long way.
- **You’re allowed to disagree with people.** Try to inform the person (respectfully) why they might be misguided and provide any links they may need to make a more informed decision. Don’t say “you’re wrong” or “that’s stupid.” Instead try to say “I disagree because…” or “I don’t think that’s accurate because…”.
- You're the boss of your social channels, and you can choose not to engage. _Consider not engaging with that person and ignoring their comments. The probability of "Twitter arguments" around various crises and controversies is high, and it is recommended that you don't fall into a fight with someone on the internet._

**If you are unsure if you should respond to someone who has responded to your posts, join the #social_media_action Slack channel and ask for feedback.**

### Tips for writing your own social media posts

<details>
  <summary markdown='span'>
    Twitter Copy Length
  </summary>

If your copy is on the longer side, try to visually break it into paragraphs or one-liners, even using emojis/bullets. Consider composing a thread if you have something longer than 280 characters, like an anecdote or a schedule. <a href="https://help.twitter.com/en/using-twitter/create-a-thread">Learn how to make a Twitter Thread here</a>.
</details>

<details>
  <summary markdown='span'>
    LinkedIn Copy Length
  </summary>

You aren't bound by character count the same way you are on Twitter, so you can think about LinkedIn posts as mini blog posts. This is especially effective if you have a perfect anecdote that you think might complement the asset you're promoting. However, keep in mind that, unless it's <i>really</i> interesting, people won't click `more` on your post (the only way to read an entire post longer than a few lines), so keep it on the short side unless you have a great hook above the `more` button.

Longer posts are easier to read if you have many paragraph breaks, so feel free to be creative with your formatting: one-liners, emojis, bullet points, etc., can help break up your text visually.
</details>

<details>
  <summary markdown='span'>
    Tone of voice
  </summary>

Tone check your copy. The most important thing is that it sounds like you, a human being. Overly contrived posts generally do not do well. If loading up your post with emojis isn't your style, don't force it.
</details>

<details>
  <summary markdown='span'>
    Visuals
  </summary>

Add an image or video! This makes a massive difference for impressions (and clicks), so it's worth the hassle. Feel free to experiment if you have an idea to personalize it in a relevant way.
</details>

<details>
  <summary markdown='span'>
    Style
  </summary>

Don't worry about writing posts that sound like news or have an editorial perspective; the brand channels will have that covered. It would be best to personalize the message if you wrote like you're learning/loving/doing something new and fun.

Instead of sharing a quote from a speaker, consider sharing the quote and adding why it personally resonated with you.
</details>

<details>
  <summary markdown='span'>
    Always use trackable links when available
  </summary>

To measure the traffic you're driving, it is paramount that you use a tracked, UTM link. If the social team is providing an enablement issue, we'll have the tracking embedded already. If you retweet GitLab posts, you're already safe. Tracked links aren't generally available to everyone. However, if you plan on using your social channels as a part of your role at GitLab, reach out to the social team in the #social_media_action Slack channel, and we can set you up with a tracking sheet and teach you how to use it.
</details>

### Profile assets (to be updated)

Profile assets for social media can be found [in the corporate marketing repository](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/social-media/profile-assets/png/assets/_general)

**Please do not use the GitLab logo as the avatar for your accounts on social. You are welcome to use our branded banners, but your profile avatar mustn't lead users to confuse your account with the official GitLab accounts.**

While you should display the fact that you work at GitLab in your bio if you intend to advocate for GitLab on social, we suggest that you avoid including the word `GitLab` in your handle. Team member advocacy is incredibly valuable, and we are lucky to have so many engaged team members, but creating an account to _solely_ post about GitLab is not adequate. Team member advocacy is so powerful that people [trust employees](https://www.scribd.com/doc/295815519/2016-Edelman-Trust-Barometer-Executive-Summary) more than brands and executives. Your advocacy is powerful when it is authentic, and having an account that only exists to promote GitLab will not ring true to others who browse your tweets.

## Tips FAQ

<details>
  <summary markdown='span'>
    Am I required to share/like a post from GitLab social channels?
  </summary>

<i>No, you're not required to participate in any way.</i>
</details>

<details>
  <summary markdown='span'>
    Am I required to have the social or comms team review my posts before I publish them?
  </summary>

<i>No, the GitLab social team will never require a review or another oversight of your own personal social media posts. If you are using suggestions from Bambu or another tool or issue we've provided, the copy is already written to be public-facing.</i>
</details>

<details>
  <summary markdown='span'>
    Am I required to identify myself as a GitLab employee on my social media channel?
  </summary>

<ul>
<li><i>No, you are not required to identify yourself as an employee for simply having a personal channel. However, if you are going to engage with community members or be considered an authority in your space, you ought to be transparent and identify yourself as a team member.</i></li>

<li><i>If you talk about work-related matters within your job responsibility area, you must disclose your affiliation with GitLab. If you have a vested interest in something you're posting on social, point it out. You can identify yourself as a team member in your profile bio, list GitLab as your employer, or mention that you're a team member in the post itself. Consider adding #LifeAtGitLab to the end of your social post to sum it up quickly and easily.</i></li>
</ul>
</details>

<details>
  <summary markdown='span'>
    I don't want to write my own posts. I'd rather share GitLab's social posts. Is there an easy way to know when they are published?
  </summary>

<i>We use a program named Bambu to help you write social posts and share online. You may already have access via Okta. If you do not have access, please reach out to the #social-advocacy Slack channel to gain access.</i>
</details>

<details>
  <summary markdown='span'>
    I don't know how to respond to a comment someone posted to me. How can I find help?
  </summary>

<i>Consider joining the `#social-advocacy` Slack channel for an easy way to connect with the social team for help.</i>
</details>

<details>
  <summary markdown='span'>
    What should I do if a friend/follower/user responds to me about controversies?
  </summary>

<i>Consider not engaging with that person and ignoring their comments. The probability of "Twitter arguments" around various crises and controversies is high and it is recommended that you don't fall into a fight with someone on the internet.</i>
</details>

<details>
  <summary markdown='span'>
    Where can I learn more about social media for our company?
  </summary>

<i>You can <a href="https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/">check out our social media handbook here</a>.</i>
</details>

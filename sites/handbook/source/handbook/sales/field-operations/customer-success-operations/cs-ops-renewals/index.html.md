---
layout: handbook-page-toc
title: "Customer Success Renewals"
description: "The Customer Success Renewals team handbook page. This covers our mission, strategies, responsibilities, and processes."
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

This page is in progress.
